# --------- PYODIDE:code --------- #

def calcule_terme():
    u=...
    n=...
    while u ...:
        n=...
        u=...
    return(n)

# --------- PYODIDE:corr --------- #

def calcule_terme():
    u=1000
    n=0
    while u<2000 :
        n=n+1
        u=u*1.04
    return(n)

# --------- PYODIDE:tests --------- #
print(calcule_terme())




# --------- PYODIDE:secrets --------- #

#assert calcule_terme() is 18

