---
author: Delphine NUNEZ 
title: Exercices python
tags:
  - suites 
  - boucles/tests
---
# Chap 2 - Suites numériques
???+ question "Exercice 1"
    Soit (u_n) la suite définie pour tout $n\in\mathbb{N}, u_n=2\sqrt{n}+2$.
    Ecrire un programme langage Python permettant de calculer pour un $n$ donné la valeur de $u_n$. Afficher les termes $u_0$ à $u_{100}$ 
   {{ IDE('ex1') }}


???+ question "Exercice 2"
 
    On place un capital de 1000€ sur un compte rémunéré à 4% par an.
    Compléter le programme Python qui calcule le nombre d’années au bout
    desquelles le capital sera doublé.

   {{ IDE('ex2') }}
    
???+ question "Exercice 3"

    Ecrire un  programme Python  qui calcule les termes de la suite $(u_n)$ définie par l’expression $u_n=\frac{5n^2-3}{n^2+7}$. Afficher en particulier les termes  $u_{10}$, $u_{100}$ et  $u_{1000}$
    Qu’observe-t-on pour des valeurs de plus en plus grandes de $n$ ?
    {{ IDE('ex3') }}

???+ question "Exercice 4"   
    
    On considère la suite $(u_n)$ définie par $u_0=1$ et pour tout entier naturel $n$ par $u_{n+1}=\frac{n+3}{3n+5}u_n$.  
    On admet que cette suite est positive et tend vers 0. Ecrire un programme Python qui affiche la plus petite valeur de $n$ pour laquelle $0 \leq u_n \leq 10^{-3}$.
    {{ IDE('ex4') }}

