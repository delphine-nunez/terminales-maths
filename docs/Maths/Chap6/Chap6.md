# Chapitre 6- Vecteurs, droites et plans de l'espace

## Objectifs
!!! note "Méthodes"  
    **Méthode 1** : Deux vecteurs $\vec{u}$ et $\vec{v}$ sont colinéaires lorsqu'il existe un réel $\lambda$ tel que $\vec{u}=\lambda.\vec{v}$. Cela permet de :  
    - Montrer que trois points sont alignés  
    - Montrer que deux droites sont parallèles  
    **Méthode 2** : Si $\vec{u}$ et $\vec{v}$  ne sont pas colinéaires. On dit que $\vec{u}$, $\vec{v}$ et  $\vec{w}$ sont coplanaires lorsqu'il existe deux réels $\lambda$ et $\mu$ tels que
     $\vec{w}=\lambda.\vec{u}+ \mu.\vec{v}$. Cela permet de :  
    - Décomposer $\vec{w}$ dans la base $(\vec{u}$, $\vec{v})$  
    - Montrer que des points sont coplanaires  
     **Méthode 3** : Les vecteurs  $\vec{u}$, $\vec{v}$ et  $\vec{w}$  sont linéairement indépendants (non coplanaires) lorsque $a.\vec{u}+b.\vec{v}+c.\vec{w}=0 \Leftrightarrow a=b=c=0$. Cela permet de définir une base de vecteurs.
     **Méthode 4** Un repère est donné par un point origine O et une base $(\vec{i}$, $\vec{j}$,$\vec{k})$. Cela permet de :  
     - Déterminer les coordonnées d'un point ou d'un vecteur ;  
     - Déterminer une représentation paramétrique de droite.   
    **Méthode 5** On peut énoncer des théorèmes sur le parallélisme de droites et de plans. Cela permet de :  
    -  Déterminer des intersections de plans, de droites, d'une droite et d'un plan ;  
    - Montrer que des plans ou des droites sont parallèles.

## Cours 
[cours Chap6](./Cours-Chap6.pdf){:target="_blank"}

## Exercices 

        
!!! question " Rappels :"
    N°2-3-4 p54    

!!! question " Vecteurs de l'espace  :"
    === "Exercices :" 
        N°17-18-25 p72  
        N°45-50-51-52p74  
        N°35-36-37-38p73
    === "Correction :" 
        [17-18-25](./corr/17-18-25.pdf){:target="_blank"}   
        [45-50](./corr/45-50.pdf){:target="_blank"}   
        [51-52](./corr/51-52.pdf){:target="_blank"}   
        [35-36-37-38](./corr/35-36-37-38.pdf){:target="_blank"}  


!!! question " Droites et plans de l'espace  :"
    === "Exercices :" 
        N°31p73 (Indication : Montrer que L,M,N forment l'intersection des plans (BCD) et (IJK))  
        N°59-60 p72  
        N°67-68-70-71p77  
        N°73-74p77  
        Représentation paramétrique d'une droite :  
        N°79-80-81p74  
        Positions relatives de droites de l'espace : 
        N°82-83-84p73  
        N°94p81  
        N°86p79  
        
    === "Correction :" 
        [31](./corr/31.pdf){:target="_blank"}   
        [59-60](./corr/59-60.pdf){:target="_blank"}   
        [67-68-70-71](./corr/67-68-70-71.pdf){:target="_blank"}  
        [73-74](./corr/73-74.pdf){:target="_blank"}  
        [79-80-81](./corr/79-80-81.pdf){:target="_blank"}   
        [82-83-84](./corr/82-83-84.pdf){:target="_blank"} 
        [94](./corr/94.pdf){:target="_blank"}  
        [86](./corr/86.pdf){:target="_blank"}   
        

!!! question " Section de plan dans un volume:"
    === "Exercices :" 
        N°62-63-65p76  
        N°104 p55  
        
    === "Correction :" 
         [62-63-65](./corr/62-63-65.pdf){:target="_blank"}   
         [104](./corr/104.pdf){:target="_blank"}   

      
