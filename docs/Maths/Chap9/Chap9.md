# Chapitre 9 - Logarithme Népérien

## Objectifs
!!! info "Méthodes "
    **Méthode 1** : Pour tout réel $a>0$, le logarithme népérien de $a$, noté $\ln(a)$, est l'unique solution de l'équation $e^x=a$. Cela permet de :  
    - créer une fonction réciproque à la fonction exponentielle.  
    - avoir une écriture exacte de la solution de l'équation $e^x=a$.  
    - Cas particuliers : $\ln(1)=0$ et $\ln(e)=1$.  
    **Méthode 2** : Pour tous réels $a$ et $b$ strictement positifs,  
    $\ln(ab)=\ln(a)+\ln(b)$  
    $\ln(\dfrac{a}{b})=\ln(a)-\ln(b)$  
    $\ln(\sqrt{a})=\dfrac{1}{2}\ln(a)$  
    Pour tout $n\in \mathbb{Z}$, $\ln(a^n)=n\ln(a)$. Cela permet de :  
    - Simplifier des calculs avec le logarithme.  
    - Résoudre des équations ou inéquation dans laquelle l'inconnue est en exposant (par ex : $0.92^n>0.4$.)  
    **Méthode 3** : La fonction $\ln$ est dérivable sur son ensemble de définition et $(\ln(x))'=\dfrac{1}{x}$. La fonction $\ln$ est strictement croissante sur $]0;+\infty[$.    
    Si la fonction $u$ est dérivable et strictement positive sur un intervalle I, alors $\ln(u)$ est dérivable sur I et  
    $(\ln(u))'=\dfrac{u'}{u}$. Cela permet de :  
    - Etudier des fonctions de la forme $\ln(u)$  
    - Etudier le signe des expressions de la forme $\ln(u)$  
    - La fonction $\ln$ étant croissante, $\ln(u)$ a les mêmes variations que $u$.  
    **Méthode 4** : Etude des limites de la fonction $\ln$

## Cours 
[cours Chap9](./Cours-Chap9.pdf){:target="_blank"}  


## Evaluation 
!!! question " Sujets "

## Démonstrations  
!!! question " Théorème des croissances comparées "
    === "Théorème  1"
        $\displaystyle\lim_{x \to +\infty} \frac{\ln(x)}{x^n}=0$
    === "Démo 1"
        On considère la fonction $f$ déginie sur $I=]0;+\infty[$ par $f(x)=ln(x)-x$. Cette fonction est la somme de fonctions dérivables sur I, elle est donc dérivable sur I et $f'(x)=\dfrac{1}{x}-1=\dfrac{1-x}{x}$.  
        La dérivée de $f$ s'annule en 1 et $f'(x)>0$ sur $]0;1[$ et $f'(x)<0$ sur $]1;+\infty[$ donc $f$ est croissante sur $]0;1[$ et décroissante sur $]1;+\infty[$, $f$ admet donc un maximum en 1 et $f(1)=\ln(1)-1=-1<0$ donc pour tout $x \in I, f(x)<0 \Leftrightarrow \ln(x)<x$.  
        Considérons la fonction $g_n(x)=\dfrac{\ln(x)}{x^n}$, cette fonction est définie sur I.
         $g_n(x)=\dfrac{1}{x^{n-1}}.\dfrac{\ln(x)}{x}$
        On a montré que pour tout $x \in I, \ln(x)<x $ donc pour $x>1, 0<g_n(x)< \dfrac{1}{x^{n-1}}$ qui tend vers 0 en $+\infty$. Donc, par le théorème des gendarmes $\displaystyle\lim_{x \to +\infty} \frac{\ln(x)}{x^n}=0$
    === "Théorème 2: "
        $\displaystyle\lim_{x \to 0^+} \ln(x).x^n=0$
    === "Démo 2"
        En posant $y=\dfrac{1}{x}$ on a  $\displaystyle\lim_{x \to 0^+} \ln(x).x^n=\displaystyle\lim_{y \to +\infty} \dfrac{\ln(\dfrac{1}{y})}{y^n}=\displaystyle\lim_{y \to +\infty} \dfrac{-\ln(y)}{y^n}=0$ d'après le théorème 1. 
        Donc $\displaystyle\lim_{x \to 0^+} \ln(x).x^n=0$


        


   
## Exercices

!!! question " Calculs algébriques :"
    === "Exercices :" 
         N°38-39 p250  
         N°40-41-42 p251  
    === "Corrigés :"  
        [38-39](./corr/38-39.pdf){:target="_blank"}  
        [40-41-42](./corr/40-41-42.pdf){:target="_blank"}  

!!! question " Equations - Inéquations :"
    === "Exercices :" 
         N°27-28p250  
         N°60-62-63 p252  
         Applications aux suites : N°47-48-49 p251
    === "Corrigés :"  
        [27-28](./corr/27-28.pdf){:target="_blank"}  
        [60-62-63](./corr/60-62-63.pdf){:target="_blank"} 
        [47-48-49](./corr/47-48-49.pdf){:target="_blank"}  

!!! question " Etude de fonctions :"
    === "Exercices :" 
         N°51 p251  
         N°37 p250  
         N°66-67 p252  
         N°104 p255  
         N°110 p256  
         N°121 p258  
         N°127p259  
    === "Corrigés :"  
        [51](./corr/51.pdf){:target="_blank"} 
        [37](./corr/37.pdf){:target="_blank"} 
        [67](./corr/67.pdf){:target="_blank"} 
        [104](./corr/104.pdf){:target="_blank"} 
        [110](./corr/110.pdf){:target="_blank"} 
        [121](./corr/121.pdf){:target="_blank"}  
        N°127 est une démonstration du théorème des croissances comparées en-tête du chapitre
        

!!! question " Type BAC :"
    === "Sujets" 
        [BAC2019](./EXO-BAC1.pdf){:target="_blank"}  
        [BAC2022-1](./EXO-BAC2.pdf){:target="_blank"}  
        [BAC2023](./BAC2023.pdf){:target="_blank"}        
    === "Corrigés" 
        [BAC2023](./Corr-BAC2023.pdf){:target="_blank"}       
        

