# Spécialité Maths Terminales


## Lien vers le manuel 
> [Livre scolaire](https://fr.calameo.com/read/000596729efdd44af7a7c?authid=P1QQmK1VcS5r "Livre Maths")

## Progression de l'année

> [progression](./Progression_2024-2025.pdf)

## Sujets Evaluations et BAC
> Dans cette section se trouve les évaluations données et les sujets types bacs à travailler.

!!! note "Evaluations année 2024-2025"
    === "Sujets" 
        [Sujet Chap 1](./Chap1/eval2024.pdf){:target="_blank"}  
        [Sujet Chap 2](./Chap2/eval2024-1.pdf){:target="_blank"}  
        [Sujet Chap 2-bis](./Chap2/eval2024.pdf){:target="_blank"}  
        [Sujet BB1](./BB1-2024.pdf){:target="_blank"}  
        [Sujet BB1-bis](./BB1-2024-Bis.pdf){:target="_blank"}  
        [Sujet Chap6](./Eval2025.pdf){:target="_blank"}
    === "Corrigés" 
        [corrigé Chap 1](./Chap1/corrige-eval2024-2.pdf){:target="_blank"}  
        [Corrigé Chap 2](./Chap2/Corr-eval-chap2.pdf){:target="_blank"}   
        [Corrigé Chap 2-bis](./Chap2/Corr-eval-chap2-bis.pdf){:target="_blank"}   
        [Corrigé BB1](./Corrige_BB1-2024.pdf){:target="_blank"}  
        [Corrigé BB1-Bis](./Corrige_BB1-2024-Bis.pdf){:target="_blank"}  
        [Corrigé Eval Chap6](./Corr-Eval6.pdf){:target="_blank"}  


!!! note "Bacs Blancs 2023-2024"
    === "Sujets"
        [BB1-G1](./BB1-G1.pdf){:target="_blank"}  
        [BB1-G2](./BB1-G2.pdf){:target="_blank"}  
        [BB2-G1](./BB2-G1.pdf){:target="_blank"}  
        [BB2-G2](./BB2-G2.pdf){:target="_blank"}
    === "Corrigés"
        [Corr - BB1-G1](./Corr-BB1-G1.pdf){:target="_blank"}  
        [Corr - BB1-G2](./Corr-BB1-G2.pdf){:target="_blank"}  
        [Corr - BB2-G1](./Corr-BB2-G1.pdf){:target="_blank"}  
        [Corr - BB2-G2](./Corr-BB2-G2.pdf){:target="_blank"}  


## Calculatrices
> Manuels des calculatrices  
!!! alerte " NOTICES "
    === "Numworks : "
        [Notice](./Numworks.pdf) 
    === "Casio : "
        [Notice](./Casio.pdf)
    === "TI : " 
        [Notice](./TI.pdf)  

## Chapitres

### Chapitre 1 : [Limites de fonctions](./Chap1/Chap1.md){:target="_blank"}

### Chapitre 2 : [Suites Numériques](./Chap2/Chap2.md){:target="_blank"}

### Chapitre 3 : [Continuité des fonctions](./Chap3/Chap3.md){:target="_blank"}

### Chapitre 4 : [Compléments sur les fonctions](./Chap4/Chap4.md){:target="_blank"}  

### Chapitre 5 : [Combinaison et dénombrement](./Chap5/Chap5.md){:target="_blank"}

### Chapitre 6 : [Vecteurs, droites et plans de l'espace](./Chap6/Chap6.md){:target="_blank"}

### Chapitre 7 : [Loi Binomiale](./Chap7/Chap7.md){:target="_blank"}

### Chapitre 8 : [Orthogonalité dans l'espace](./Chap8/Chap8.md){:target="_blank"}


### Chapitre 9 : [Logarithme Népérien](./Chap9/Chap9.md){:target="_blank"}

### Chapitre 10 : [Equations différentielles](./ZChap10/Chap10.md){:target="_blank"}

### Chapitre 11 : [Fonctions trigonométriques](./ZChap11/Chap11.md){:target="_blank"}
## Epreuves

### Epreuve écrite : 4 h <br>
Le sujet comporte quatre exercices indépendants les uns des autres, qui permettent d'évaluer les connaissances et compétences des candidats.<br>

Le sujet précise si l'usage de la calculatrice, dans les conditions précisées par les textes en vigueur, est autorisé.

### Grand Oral

> Grille évaluation 
> [grille](./Grille_GO.pdf)

Voyages au pays des maths (Arte) : Vidéos sur des thèmes pouvant être utilisés pour le Grand Oral. 

- [Sur la route de l'infini](https://www.arte.tv/fr/videos/097454-005-A/voyages-au-pays-des-maths/ "Sur la route de l'infini")
- [Les irrationnnels](https://www.arte.tv/fr/videos/097454-009-A/voyages-au-pays-des-maths/ "Les irrationnels")  
- [notions infinitesimales](https://www.arte.tv/fr/videos/097454-003-A/voyages-au-pays-des-maths/ "calcul infinitésimal")  

Chaine de Mickaël Launay 
- [MicMaths](https://www.youtube.com/@Micmaths "Micmaths")

