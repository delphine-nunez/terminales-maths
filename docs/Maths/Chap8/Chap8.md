# Chapitre 8 - Orthogonalité dans l'espace

## Objectifs
!!! info "Méthodes "
    **Méthode 1** : Dans l'espace, le produit scalaire de deux vecteurs non-nuls $\vec{u}$ et $\vec{v}$ se définit par : $\vec{u}.\vec{v}=\lvert \vec{u}\rvert\times \lvert \vec{v}\rvert \times \cos(\vec{u},\vec{v})$. Cela permet de :  
    - Calculer des longueurs ;  
    - Calculer des longueurs d'angles ;  
    - Prouver l'orthogonalité de vecteurs.  
    **Méthode 2** : Dans l'espace, une base $(\vec{i},\vec{j},\vec{k})$ est orthonormée lorsque les vecteurs de la base ont tous une norme égale à 1 et qu'ils sont orthogonaux deux à deux. Cela permet de :  
    - Définir un repère orthonormé ;  
    - Calculer des longueurs dans l'espace ;  
    - Calculer des produits scalaire à l'aide des coordonnées des vecteurs.  
     **Méthode 3** : Pour un plan défini par $ax+by+cz+d=0$, les coordonnées d'un vecteur normal à ce plan sont $\vec{n}=\begin{pmatrix} a \\ b \\c \end{pmatrix}$. Cela permet de  :  
     - Déterminer l'équation cartésienne d'un plan ;  
     - Déterminer la représentation paramétrique d'une droite orthogonale à un plan ;  
     - Etudier l'orthogonalité entre les droites et plans de l'espace.  
     **Méthode 4** : Pour trouver l'intersection de droites et de plans, on résout le système fourni avec les équations. Cela permet de :  
     - Trouver les coordonnées du projeté orthogonal d'un point sur une droite ou sur un plan ;  
     - Déterminer la distance entre un point et une droite ou un plan.  
     **Méthode 5** : Autres formules du produit scalaire :  
     - Avec les coordonnées dans un repère orthonormé : $\vec{u}.\vec{v}=x.x'+y.y'+z.z'$  
     - Formules de polarisation : $\vec{u}.\vec{v}=\frac{1}{2}[\lvert \vec{u}\rvert^2 +\lvert \vec{v}\rvert^2-\lvert \vec{u}-\vec{v}\rvert^2]$  
     $\vec{u}.\vec{v}=\frac{1}{2}[\lvert \vec{u}+\vec{v}\rvert^2-\lvert \vec{u}\rvert^2 -\lvert \vec{v}\rvert^2]$  
        - Avec H le projeté orthogonal de C sur (AB)  
        $\overrightarrow{AB}.\overrightarrow{AC}= AB\times AH$ si $(\overrightarrow{AB},\overrightarrow{AC})\in [-\dfrac{\pi}{2}; \dfrac{\pi}{2}]$  
        $=-AB\times AH$ sinon.  

## Cours 
[cours Chap8](./Cours-Chap8.pdf){:target="_blank"}  
   
## Démonstrations

!!! note "Equation cartésienne du plan :" 
    === "Propriété : " 
         Un plan a pour équation cartésienne $ax+by+cz+d=0$ avec le vecteur normal  $\vec{n}=(a,b,c)$  
    === "Démonstration :"
        $\vec{n}$ est normal au plan P contenant le point A. Donc pour tout $M(x,y,z)$ du plan P, le produit scalaire $\overrightarrow{AM}.\vec{n}=0$  
        $a(x-x_A)+b(y-y_A)+c(z-z_A)=0 \Leftrightarrow ax+by+cz-(ax_A+by_A+cz_A)=0$.  
        En posant $d=-(ax_A+by_A+cz_A)$ on a bien l'équation cartésienne de P:  $ax+by+cz+d=0$

!!! note "Distance et projeté :" 
    === "Propriété : " 
        Si H est le projeté orthogonal de A sur un plan P, alors la distance de A à P est AH.
    === "Démonstration :" 
        Soit M un point du plan différent de H. Le triangle AHM est rectangle en H, donc l'hypoténuse AM>AH. Ainsi AH est la plus petite longueur de A au plan P.
## Exercices

!!! question " Produit scalaire analytique :"
    === "Exercices :" 
         N°22-30 p102  
         N°64-65 p105  
         N°79 p107
    === "Corrigés :"      
        [22-30](./corr/22-30.pdf){:target="_blank"}  
         [64-65](./corr/64.65.pdf){:target="_blank"}  
         [79](./corr/79.pdf){:target="_blank"}   
    === "Exercices :" 
         N°34-36 p103  
         N°66 p105  
         N°74-75 p106       
    === "Corrigés :"  
        [34-36](./corr/34-36.pdf){:target="_blank"}  
        [66](./corr/66.pdf){:target="_blank"}  
        [74-75](./corr/74-75.pdf){:target="_blank"}  

!!! question "Calcul d'angles :"
    === "Exercices"
        N°29-28-27 p105  
        N°53-54-57 p106  
    === "Corrigés"  
         [29-28-27](./corr/29-28-27.pdf){:target="_blank"}  
         [53-54-57](./corr/53-54-57.pdf){:target="_blank"}  
           

!!! question "Projection orthogonale - Distance à un plan :"
    === "Exercices" 
        N°38-39-40-42-43 p106  
        N°89 p108  
        N°100 p109  
        
    === "Corrigés"  
        [38-39-40-42-43](./corr/38-39-40-42-43.pdf){:target="_blank"}  

!!! question "Equations de sphères - Intersection de plans :"
    === "Exercices" 
        N°44-45 p106  
        N°110 p112  
        Intersection de plans :   
        N°83p108  
        N°106 p111  
    === "Corrigés"  
        [44-45](./corr/44-45.pdf){:target="_blank"}  
        [110](./corr/110.pdf){:target="_blank"}   
        [83-106](./corr/83-106.pdf){:target="_blank"}  
        



!!! question "Problèmes :"
    === "Exercices" 
        N°105p 110  
        N°117 p115  
    === "Corrigés"  
          A venir