# Chapitre 1 - Limites de fonctions
## Objectifs
!!! info " Méthodes "
    **méthode 1** : Définition des limites.   
    Cela permet de :  
     - Etudier le comportement de $f$ même lorsque la fonction n'est pas définie (pour une valeur interdite).   
     - Déterminer des asymptotes verticales, horizontales  
     **méthode 2** : Opérations sur les limites  
     **méthode 3** : Théorèmes de comparaison  
     **méthode 4** : Théorème des croissances comparées
   
    
    
## Cours 

[cours Chap1](./Cours-Chap1.pdf){:target="_blank"}

### Démonstrations du chapitre 1
!!! attention "Démonstration de la limite de $e^x$ en $+\infty$"
    ===  "Propriété : "
        $$\displaystyle\lim_{x \to +\infty} {e^x}=+\infty$$
    
    ===  "Démonstration :"
        **Pré-requis : théorème de comparaison** <br>
        Soit la fonction $g$ définie sur $\mathbb{R}$ par $g(x)=e^x-x$ <br>
        $g$ est dérivable sur $\mathbb{R}$ et $g'(x)=e^x-1$ <br>
        $g'(x)>0 \Leftrightarrow x>0$. <br>
        $g$ admet donc un minimum en 0 qui vaut $g(0)=1$. Donc $g(x)>0$ pour tout $x\in \mathbb{R}$<br>
        Donc pour tout réel $x$, $e^x>x$ <br>
        Par comparaison: 
        $\displaystyle\lim_{x \to +\infty} {e^x}>\displaystyle\lim_{x \to +\infty} {x}$ <br>
        donc         $\displaystyle\lim_{x \to +\infty} {e^x}=+\infty$ **CQFD**
       
!!! attention  "Démonstration de la limite de $e^x$ en $-\infty$"  
    ===  "Propriété : "
        $$\displaystyle\lim_{x \to -\infty} {e^x}=0$$
    
    ===  "Démonstration :"
        **Pré-requis : limite en $+\infty$ et composition de limite** <br>
        $\displaystyle\lim_{x \to -\infty} {e^x}=\displaystyle\lim_{x \to +\infty} {e^{-x}}$<br>
        $=\displaystyle\lim_{x \to +\infty} \frac{1}{e^x}=0$ **CQFD**

!!! attention "Démonstration du théorème des croissances comparées"
    === "Théorème 1: "
        $\displaystyle\lim_{x \to +\infty} \frac{e^x}{x^n}=+\infty$
        
    === "Démonstration 1"
        **Pré-requis : Opérations sur les limites, composition de limites, théorème des gendarmes**<br>
        **Cas $n=1$**<br>
        Soit la fonction $g$ définie sur $\mathbb{R}$ par $g(x)=e^x-\frac{x^2}{2}$ <br>
        $g$ est dérivable sur $\mathbb{R}$ et $g'(x)=e^x-x$ <br>
        On a montré dans la démonstration de la limite de $e^x$ que $g'(x)>0$ donc $g$ est croissante sur $\mathbb{R}$.<br>
        donc $\forall x>0$, on a $g(x)>g(0) \Leftrightarrow g(x)>1>0$ donc $e^x>\frac{x^2}{2} \Rightarrow \frac{e^x}{x}>\frac{x}{2}$<br>
        Par comparaison, comme $\displaystyle\lim_{x \to +\infty} \frac{x}{2}=+\infty$ alors $\displaystyle\lim_{x \to +\infty} \frac{e^x}{x}=+\infty$ <br>
        
        **Cas $n> 1$** <br>
        $\frac{e^x}{x^n}=(\frac{e^{\frac{x}{n}}}{\frac{x}{n}})^n\times(\frac{1}{n})^n$<br>
        Posons $X=\frac{x}{n}$ alors $(\frac{e^{\frac{x}{n}}}{\frac{x}{n}})^n\times(\frac{1}{n})^n=(\frac{e^X}{X})^n\times(\frac{1}{n})^n$. <br>
        On a $\displaystyle\lim_{x \to +\infty}X=+\infty$ et on a vu que $\displaystyle\lim_{X \to +\infty} \frac{e^X}{X}=+\infty$ donc par produit $\displaystyle\lim_{X \to +\infty} \frac{e^X}{X}\times(\frac{1}{n})^n=+\infty$ <br>
        Et par composition on a         $\displaystyle\lim_{x \to +\infty} \frac{e^x}{x^n}=+\infty$
        **CQFD**
        
    === "Théorème 2 : "
        $\displaystyle\lim_{x \to -\infty} {x^n e^x}=0$
        
    === "Démonstration 2"
        **Pré-requis : Théorème 1, opérations sur les limites, composition de limites, théorème des gendarmes**<br>
        $x^n e^x=\frac{x^n}{e^{-x}}$ <br>
        $\displaystyle\lim_{x \to -\infty} {x^n e^x}=\displaystyle\lim_{x \to -\infty}\frac{x^n}{e^{-x}}=\displaystyle\lim_{x \to +\infty}\frac{(-x)^n}{e^{x}}=\displaystyle\lim_{x \to +\infty}(-1)^n\frac{x^n}{e^{x}}$<br>
        On sait que $\displaystyle\lim_{x \to +\infty} \frac{e^x}{x}=+\infty$ donc par inverse, $\displaystyle\lim_{x \to +\infty}\frac{x^n}{e^{x}}=0$. De plus, $-1\leq (-1)^n\leq 1$ donc $-\frac{x^n}{e^{x}}<(-1)^n\frac{x^n}{e^{x}}<\frac{x^n}{e^{x}}$ par produit $\displaystyle\lim_{x \to +\infty}-\frac{x^n}{e^{x}}=\displaystyle\lim_{x \to +\infty}\frac{x^n}{e^{x}}=0$<br>
        Donc par le théorème des gendarmes         $\displaystyle\lim_{x \to -\infty} {x^n e^x}=0$ **CQFD**
      
        
## Exercices 
!!! example "Activités :" 
    A page 162 <br>
    B page 162

!!! question "Recherche de limites :"
    === "Animations géogébra :"
        - Limite finie  
        <iframe scrolling="no" title="Limite d'une fonction_ Limite finie en l'infini" src="https://www.geogebra.org/material/iframe/id/nkgwn9qv/width/1429/height/640/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1429px" height="640px" style="border:0px;"> </iframe>

        - Limite infinie  
        <iframe scrolling="no" title="Copie de Limite d'une fonction: Limite infinie en l'infini" src="https://www.geogebra.org/material/iframe/id/eytsespp/width/1429/height/640/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="1429px" height="640px" style="border:0px;"> </iframe>
    ===  "Exercices  :"
        N°17 p176 <br>
        N°20 p176 <br>
        N°21 p176 <br>
        
    ===  "Corrigé :"
        [Correction 17-20-21](./corr/17-20-21.pdf){:target="_blank"}

    === "Exercices en autonomie :" 
        Exercice : En vous aidant du [graphe](https://www.geogebra.org/m/TBQMtn9B), donnez les équations réduites des asymptotes à la courbe.  
        Exercices :  
        Consigne de saisie : écrire +inf pour désigner $+\infty$ écrire -inf pour désigner $-\infty$  
        [asymptote-limite1](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=asympt2hv&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"}    
        [asymptote-limite2](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=asympt1hv&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"}     
        [Limite de fonctions usuelles niveau 1](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=formules&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"} La fonction $\ln$ sera étudiée plus tard dans l'année  
        [Limite de fonctions usuelles niveau 2](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=formules2&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"} La fonction $\ln$ sera étudiée plus tard dans l'année  
        [Limite d'autres fonctions usuelles](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=formules0&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"} La fonction $\ln$ sera étudiée plus tard dans l'année  
        [Limite d'autres fonctions usuelles](https://wims.univ-cotedazur.fr/wims/wims.cgi?session=2NBB0BAF73.2&lang=fr&cmd=new&module=H6%2Fanalysis%2Foeflimite.fr&exo=formules02&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){:target="_blank"} La fonction $\ln$ sera étudiée plus tard dans l'année  
!!! example " Notion d'asymptotes" 
    - Le comportement à l'infini d'une fonction permet de définir une asymptote horizontale. Si $\displaystyle{\lim_{x\to +\infty}f(x)=l}$ alors la courbe $\mathcal{C}_f$ admet une asymptote horizontale d'équation $y=l$.  
        <iframe scrolling="no" title="limite fonction asymptote horizontale" src="https://www.geogebra.org/material/iframe/id/zxNJj6Cb/width/670/height/603/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="670px" height="603px" style="border:0px;"> </iframe>  
    - Le comportement proche d'une valeur $$a$$ permet de définir une asymptote verticale. Si $\displaystyle{\lim_{x\to a^+}f(x)=\infty}$ ou/et  $\displaystyle{\lim_{x\to a^-}f(x)=\infty}$ alors la courbe $\mathcal{C}_f$ admet une asymptote verticale d'équation $x=a$. Le plus souvent $a$ est une valeur interdite. C'est à dire que la fonction $f$ n'est pas définie en $a$.  
    <iframe scrolling="no" title="asymptote verticale" src="https://www.geogebra.org/material/iframe/id/DhVCJkvv/width/530/height/669/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="530px" height="669px" style="border:0px;"> </iframe>  

!!! question "Formalisme des limites :"
    === "Exercices :"
        N°38 p178 
    === "Indice :"
        Pour $A$ donné, il faut résoudre l'inéquation $f(x)>A$
    === "Corrigé :"  
        [38](./corr/38.pdf){:target="_blank"}  
       <br>
!!! question "Opérations sur les limites :"
    

    === "Exercices :"
        N°24 p177 <br>
        N°25 p177 <br>
        N°26 p177 <br>
        N°52 p180 
    === "Indice :"
        Utiliser les formules du cours <br>
        Tracer la courbe représentative à la calculatrice pour conjecturer le résultat.
    === "Corrigé :"
        [Correction 24-25-26](./corr/24-25-26-54.pdf){:target="_blank"}  
    === " Exercices en autonomie :"  
        [calcul de limites à l'infini](https://mathenpoche.sesamath.net/?page=terminale#terminale_2_4_3_sesabibli/5e69e21ac01e0c238782b199){:target="_blank"}   
        [calcul de limites en un réel](https://mathenpoche.sesamath.net/?page=terminale#terminale_2_4_3_sesabibli/5e69e3bac01e0c238782b19a){:target="_blank"}  

!!! question "Formes Indéterminées :"
    === "Exercices :"
        N°27 p177 <br>
        N°28 p177 <br>
    === "Indice :"
        Utiliser les méthodes vues page 9 du cours
        Tracer la courbe représentative à la calculatrice pour conjecturer le résultat.
    === "Corrigé :"
         [27-28](./corr/27-28.pdf){:target="_blank"}  
    
!!! question "Limite d'une composée de fonctions :" 
    === "Exercices : "
        > Déterminer les limites suivantes :  
        - $\displaystyle\lim_{x \to +\infty} \sqrt{x^2+3x+2}$  
        - $\displaystyle\lim_{x \to +\infty} e^{-x^3-5x^2+2}$  

!!! notice "Exercices en plus :"
    === "Exercices :"
        > Calculs de limites en $+\infty$ et $-\infty$  
        - des Fonctions polynômes   
        $f(x)=5x^3+2x^2-6x+5$  
        $g(x)=-4x^5+2x^2+10$  
        - Fonctions rationnelles   
        $h(x)=\displaystyle{\lim_{x \to -\infty}{\dfrac{2x^2+x-3}{3x+4}}}$  
        $k(x)=\displaystyle{\lim_{x \to +\infty}{\dfrac{8x^3-5x+4}{3x^3+2x^2-1}}}$

        > Exercices interactifs : [Limites de fonctions composées](https://mathenpoche.sesamath.net/?page=terminale#terminale_2_4_3_sesabibli/5e6cd82ad1d2d74c7d1b3ddf){:target="_blank"}  
    
!!! question "Comparaisons - Théorème des gendarmes :"
   
    === "Exercices :"
        N°75 p183 <br>
        N°77 p183 <br>
        N°80 p183 
    === "Indice :"
        Cours + les méthodes vues page 9 du cours (Ex 80)
        Tracer la courbe représentative à la calculatrice pour conjecturer le résultat.
    === "Corrigé :"
         
         [75-77-80](./corr/75-77-80.pdf){:target="_blank"}  

    === "Exercices en autonomie :"
        > Déterminer les limites suivantes :  
        $f(x)=\displaystyle{\lim_{x \to +\infty}{\dfrac{\sin(x)+3}{x}}}$  
        $g(x)=\displaystyle{\lim_{x \to +\infty}{\dfrac{2x+\cos(x)}{x-1}}}$  
        $h(x)=\displaystyle{\lim_{x \to +\infty}{\dfrac{\cos(x)	}{x^2}}}$  
        $k(x)=\displaystyle{\lim_{x \to +\infty}{\dfrac{2+\cos(x)}{x-\sin(2x)}}}$  
        [corrigé Vidéo de Ans Hamble](https://www.youtube.com/watch?v=jOyBpZ16tYE)

!!! question "Demonstration limites de l'exponentielle : "
    === "Exercices :"
        N°91 p184 <br>
    
!!! question "Théorèmes des croissances comparées :"
    
    === "Exercices :"
        N°84 p183 <br>
        Demonstration des théorèmes (faits ensemble)
    === "Indice :"
        Cours
        Tracer la courbe représentative à la calculatrice pour conjecturer le résultat.
    === "Corrigé :"
        A venir
         <!---
         [84](./corr/84.pdf){:target="_blank"}  -->  
    === "Exercices en Autonomie :"
        >Déterminer $\displaystyle{\lim_{x \to +\infty}{\dfrac{e^{x}+x}{e^{x}-x^{2}}}}$  
        [corrigé de Yvan Monka ](https://www.youtube.com/watch?v=GoLYLTZFaz0)

!!! danger "Evaluations" 
    === "Travail de groupe" 
        à venir
        <!-- [Sujet](./-->