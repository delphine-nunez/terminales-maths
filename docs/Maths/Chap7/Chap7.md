# Chapitre 7 -  Loi Binomiale

## Objectifs
!!! info "Méthodes "
    **Méthode 1** : Une épreuve de Bernoulli de paramètre $p$ est une expérience aléatoire à deux issues dont l'une d'elles est appelée "succès", de paramètre $p$. Un schéma de Bernoulli de paramètres $n$ et $p$ est la répétition de $n$ épreuves de Bernoulli de paramètres $p$ indépendantes. Cela permet de :  
    - Modéliser de nombreuses expériences aléatoires dans un contexte de répétitions d'expériences à deux issues.  
    - Faire des calculs de probabilités dans construire d'arbre pondéré correspondant à la situation : chaque chemin menant à $k$ succès correspond à une probabilité $p^k(1-p)^{n-k}$ et il y a $\binom{n}{k}$ chemins menant à k succès.  
    **Méthode 2** : Une variable aléatoire X qui compte le nombre de succès dont la probabilité est $p$ dans un schéma de Bernoulli de $n$ épreuves suit une loi binomiale de paramètres $n,p$. Cela permet de :  
    - Calculer la probabilité d'obtenir $k$ succès : pour $0\leq k\leq n, P(X=k)=\binom{n}{k} p^k (1-p)^{n-k}$  
    - Déterminer un intervalle I tel que $P(X\in I)\leq \alpha$ ou $P(X\in I)\geq 1-\alpha$ avec $\alpha \in [0;1]$.  
    **Méthode 3** : L'espérance et la variance d'une variable aléatoire X qui suit une loi binomiale de paramètres $n,p$ sont : $E(X)=np ; V(X)=np(1-p)$. Cela permet de :  
    - Calculer l'espérance d'une variable aléatoire et d'en interpreter le sens dans le contexte,  
    - Calculer la variance d'une variable aléatoire,  
    - Calculer l'écart-type : $\sigma(X)=\sqrt{V(X)}=\sqrt{np(1-p)}$.  

    
## Cours 
[cours Chap7](./Cours_Chap7.pdf){:target="_blank"}  
[résumé Chap7](./resume_proba_discrete_loi_binomiale.pdf){:target="_blank"}  
[résumé proba conditionnelle](./resume1ere.pdf){:target="_blank"}  

## Exercices

!!! question " Loi Binomiale:"
    === "Exercices :" 
         Activité A p352  
         N°27-28 p364  
         N°29-31-32-33 p364  
         N°54-55p366  
         N°59-60 p367  
         N°64-65-66 p367  
         N°67-68 p367-368  
         
    === "Correction :" 

         [27-28-29](./corr/27-28-29.pdf){:target="_blank"}   
         [31-32-33](./corr/31-32-33.pdf){:target="_blank"}   
         [54-55](./corr/54-55.pdf){:target="_blank"}   
         [59-60](./corr/59-60.pdf){:target="_blank"}   
<!--
         [64-65-66](./corr/64-65-66.pdf){:target="_blank"}  
         [67-68](./corr/67-68.pdf){:target="_blank"}  
-->
!!! info "Exercices entrainement" 
    === "Mathaléa"
        [Lien](https://capytale2.ac-paris.fr/web/c/cb8c-5132493){:target="_blank"}  
!!! note "TP Planche de Galton"
    === "Notebook Capytale"
        [Lien](https://capytale2.ac-paris.fr/web/c/5177-5129524){:target="_blank"}  

!!! question " Echantillonage:"
    === "Exercices :" 
        N°89-90-91 p 371  
        N°93-94 p371  
        N°97-98 p371  
         
    === "Correction :" 
        [89-90-91-93-94](./corr/89-90-91-93-94.pdf){:target="_blank"}  
        [97-98](./corr/97-98.pdf){:target="_blank"}  


!!! question " Type BAC"
    [Sujet](./SujetBAC-Probabilite.pdf){:target="_blank"}  