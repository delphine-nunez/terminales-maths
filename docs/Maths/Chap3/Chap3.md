# Chapitre 3 - Continuité de fonctions

## Objectifs
!!! info "Méthodes" 
    **Méthode 1** : $f$ est continue en $a$ si $\displaystyle\lim_{x\to a}f(x)$ existe et est égale à $f(a)$. Cela permet de :  
    - savoir si la courbe représentative de $f$ peut se tracer "sans lever le crayon".  
    - appliquer certains théorèmes  
    - dire que toute fonction dérivable est continue.   
    **Méthode 2** : Le théorème des valeurs intermédiaires permet de savoir si l'équation $f(x)=k$ admet des solutions.  
    Son corollaire, le théorème de la bijection, permet de savoir si cette solution est unique.  
    **Méthode 3** : Application aux suites ; le théorème du point fixe donne 
    " Soit $f$ est une fonction continue de I à valeurs dans I, et $u_{n+1}=f(u_n)$. Si $u_n$ converge alors sa limite $l$ est solution de $f(x)=x$.
    
## Cours 
[cours Chap3](./Cours-chap3.pdf){:target="_blank"}

## Exercices 

!!! question "Notion de continuité"
    === "Exercices :" 
        Activité A p192  
        N°23-24-25 p202
    === "Correction :"
         [23-24-25](./corr/23-24-25.pdf){:target="_blank"} 

!!! question " Théorème des valeurs intermédiaires"
    === "Exercices :"
        Activité B p192  
        N°63-64 p205  
        N°62-66 p205 
    === "Correction : "
        [63-64](./corr/63-64.pdf){:target="_blank"}  
        [62-66](./corr/62-66.pdf){:target="_blank"}  
!!! info " Algorithme - dichotomie :"  
    === " Programmation :"
        [TP](https://capytale2.ac-paris.fr/web/c/9855-4091511)
!!! question " Application aux limites - Théorème du point fixe"
    === "Exercices :"
        Activité C p193  
        N°79-80 p206  
    === "Correction : "
        [79-80](./corr/79-80.pdf){:target="_blank"} 


