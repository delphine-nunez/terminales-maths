# Chapitre 3 - Matrices et application aux graphes
!!! info "Cours" 
     [Cours](./Cours-chap3.pdf)   


    
!!! note "Construction de matrices"
    ![Exo](./IMG_0907.jpeg)
    
    
!!! note "Calculs matriciels"
    N°20-22-23-24 p190  
    N°44 p192
    N°47 p193

!!! note "Inverse de matrices et applications"
    N°26-28-29 p190
    N°49-50-52-53 p193


!!! note "Transformations géométriques "
    En construction
!!! note "Les graphes "
    En construction
