# Chapitre 4 - Nombres complexes - Partie Géométrique

!!! info "Cours" 
    Le cours [Cours](./Cours-Chap4.pdf)

!!! note "Rappels"
    N°1-5-6 p48  

!!! note " Géométrie et complexes"
    A p50  
    N°26-27-28 p66  

!!! note " Module et argument"
    N°29-30-31-32 p67  
    N°60 p69  
    N°68 p70
!!! note " forme trigonometrique"  
    N°33-34 p67  
    N°73-74-75 p70  
!!! note " forme exponentielle"
    N°36 p67  
    N°92-93-91p72
!!! note " applications à la géométrie"
    N°96-98 p73  
    N°82-83p71  
    N°37-38-39-40p 67  
    N°112-116-118p74   
    N°121-122-123p75

!!! note "Racines de l'unité "
    N°44-45p67   
    N°125-126-127p75
