# Option Mathématiques Expertes


Page regroupant les documents des cours de l'option Maths Expertes. <br>
L'option est évaluée en controle continu.

## Manuel Numérique 
[livre scolaire](https://fr.calameo.com/read/0005967295d0b5d5c47f6?authid=nDfde6HMoRP5 "Livre spé Maths"){:target="_blank"}


## Chapitres

### Chapitre 1 : [Nombres Complexes - Partie algebrique](./Chap1/Chap1.md){:target="_blank"}  

### Chapitre 2 : [Divisibilité dans l'ensemble des entiers relatifs](./Chap2/Chap2.md){:target="_blank"}

### Chapitre 3 : [Calculs matriciels et applications aux graphes](./Chap3/Chap3.md){:target="_blank"}  

### Chapitre 4 : [Nombres Complexes - Partie géométrique](./Chap4/Chap4.md){:target="_blank"}  
