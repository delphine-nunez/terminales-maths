# Chapitre 1 - Nombres complexes - Partie Algébrique


!!! info "Cours" 
    [Cours](./Cours-chap1.pdf)

!!! example "Activité de découverte"
    Activité A p18  
    Remarque : cette activité est difficile par rapport au reste du chapitre, il est interessant de voir comment l'idée d'un nombre au carré négatif.  

!!! note "Forme algébrique"
    N°26-27-29p34  
    N°61-62p36  
    N°64p36  
    
!!! note "Formule de Newton" 
    N°66p37 (1 et 3.) et 67p37 (2.)  
    

!!! note "Nombre complexe conjugué-inverse et quotient de nombres complexes"
    N°79p38  
    N°80-81p38  
    N°84-85-86p38
    
!!! note "Equations" 
    === "Video"
        <iframe width="560" height="315" src="https://www.youtube.com/embed/M6o5CRYfNxA?si=dkDvllhm82QJowCm" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
    === "Exerices"
        N°31-32p34  
        N°33-34p34  
        N°94-96p39  
        N°43-44p35  
        N°110-111p40  
        N°113p41

!!! note "Factorisation de polynômes"
    N°47-49-50p35  
    N°118-119p41  
    N°121p41  
    N°124-125p42
